export const ENV = {
  apiEndpoint: 'https://local-endpoint.com',
  environmentName: 'Local Environment',
  ionicEnvName: 'local'
};
