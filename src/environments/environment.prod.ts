export const ENV = {
  apiEndpoint: 'https://prod-endpoint.com',
  environmentName: 'Production Environment',
  ionicEnvName: 'prod'
};
