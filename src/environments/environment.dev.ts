export const ENV = {
  apiEndpoint: 'https://dev-endpoint.com',
  environmentName: 'development Environment',
  ionicEnvName: 'dev'
};
