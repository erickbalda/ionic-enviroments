var path = require('path');
var useDefaultConfig = require('@ionic/app-scripts/config/webpack.config.js');
var env = process.env.ENV;

// Asignacion de enviroment a default en caso de no ser definido
if (env == undefined){
  env = process.env.IONIC_ENV;
}

useDefaultConfig.dev.resolve.alias = {
  "@app/env": path.resolve('./src/environments/environment.dev.ts')
};

useDefaultConfig.prod.resolve.alias = {
  "@app/env": path.resolve('./src/environments/environment.prod.ts')
};

// Asignacion de archivo custom enviroment 
  useDefaultConfig[env] = useDefaultConfig.dev;
  useDefaultConfig[env].resolve.alias = {
    "@app/env": path.resolve('./src/environments/environment' + '.' + env + '.ts')
  };

module.exports = function () {
  return useDefaultConfig;
};